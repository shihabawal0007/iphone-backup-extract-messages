# iphone-backup-extract-messages



## Getting started

Start by cloning the project

```
git clone https://gitlab.com/shihabawal0007/iphone-backup-extract-messages.git
```

Copy the ```3d0d7e5fb2ce288813306e4d4636395e047a3d28``` file from the unencrypted back to the root folder of this project then install packages and run 

```
npm install
npm start
```

Inspired by [this project](https://github.com/harrisoff/ios-message-export)
